<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN" sourcelanguage="en_US">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/aboutdialog.ui" line="26"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.ui" line="93"/>
        <source>Biometric Manager</source>
        <translation>生物特征管理工具</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.ui" line="151"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.ui" line="196"/>
        <source>Contributor</source>
        <translation>贡献者</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.ui" line="278"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;         Biometric Manager is a supporting software for managing biometric identification which is developed by Kylin team.  It mainly contains biometirc verification management, biometirc service management, biometric device&apos;s driver management and biometirc features management, etc.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;       &lt;/span&gt;&lt;a name=&quot;textarea-bg-text&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;A&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;ll functions of the software are still being perfected. Please look forward to it. &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;         生物识别管理工具是由麒麟团队开发的一款用于管理生物识别的辅助软件。主要功能包括生物识别认证管理、生物识别服务管理、生物识别设备驱动管理以及生物识别特征的管理等功能。&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;        各项功能目前还在不断完善中，敬请期待。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.ui" line="294"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;© 2016-2017 lihao &amp;lt;lihao@kylinos.cn&amp;gt;&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;© 2017-2018 yanghao &amp;lt;yanghao@kylinos.cn&amp;gt;&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;         Biometric Manager is a supporting software for managing biometric identification which is developed by Kylin team.  It mainly contains biometirc verification management, biometirc service management, biometric device&apos;s driver management and biometirc features management, etc.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;       &lt;a name=&quot;textarea-bg-text&quot;&gt;&lt;/a&gt;All functions of the software are still being perfected. Please look forward to it. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;         生物识别管理工具是由麒麟团队开发的一款用于管理生物识别的辅助软件。主要功能包括生物识别认证管理、生物识别服务管理、生物识别设备驱动管理以及生物识别特征的管理等功能。&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;        各项功能目前还在不断完善中，敬请期待。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;         生物识别管理工具是由麒麟团队开发的一款用于管理生物识别的辅助软件。主要功能包括生物识别认证管理、生物识别服务管理、生物识别设备驱动管理以及生物识别特征的管理等功能。&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;        各项功能目前还在不断完善中，敬请期待。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;         生物识别管理工具是由麒麟团队开发的一款用于管理生物识别的辅助软件。主要功能包括生物识别认证管理、生物识别服务管理、生物识别设备驱动管理以及生物识别特征的管理等功能。&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;        各项功能目前还在不断完善中，敬请期待。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.ui" line="338"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
</context>
<context>
    <name>ContentPane</name>
    <message>
        <location filename="../src/contentpane.ui" line="14"/>
        <source>Form</source>
        <translation>表单</translation>
    </message>
    <message>
        <location filename="../src/contentpane.ui" line="48"/>
        <location filename="../src/contentpane.ui" line="68"/>
        <location filename="../src/contentpane.ui" line="116"/>
        <location filename="../src/contentpane.ui" line="134"/>
        <location filename="../src/contentpane.ui" line="164"/>
        <location filename="../src/contentpane.ui" line="188"/>
        <location filename="../src/contentpane.ui" line="266"/>
        <location filename="../src/contentpane.ui" line="357"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/contentpane.ui" line="212"/>
        <source>Device Status:</source>
        <translation>设备状态：</translation>
    </message>
    <message>
        <location filename="../src/contentpane.ui" line="146"/>
        <source>Default Device:</source>
        <translation>默认设备：</translation>
    </message>
    <message>
        <location filename="../src/contentpane.ui" line="200"/>
        <source>Verify Type:</source>
        <translation>验证类型：</translation>
    </message>
    <message>
        <location filename="../src/contentpane.ui" line="416"/>
        <source>Clean</source>
        <translation>清空</translation>
    </message>
    <message>
        <source>Biometric Type:</source>
        <translation type="vanished">生物识别类型：</translation>
    </message>
    <message>
        <location filename="../src/contentpane.ui" line="92"/>
        <source>Device Short Name:</source>
        <translation>设备简称：</translation>
    </message>
    <message>
        <location filename="../src/contentpane.ui" line="290"/>
        <source>Device Full Name:</source>
        <translation>设备全称：</translation>
    </message>
    <message>
        <location filename="../src/contentpane.ui" line="230"/>
        <source>Bus Type:</source>
        <translation>总线类型：</translation>
    </message>
    <message>
        <location filename="../src/contentpane.ui" line="248"/>
        <source>Identification Type:</source>
        <translation>识别类型：</translation>
    </message>
    <message>
        <location filename="../src/contentpane.ui" line="176"/>
        <source>Storage Type:</source>
        <translation>存储类型：</translation>
    </message>
    <message>
        <source>Status:</source>
        <translation type="vanished">状态：</translation>
    </message>
    <message>
        <source>Biometric Feature List</source>
        <translation type="vanished">特征列表</translation>
    </message>
    <message>
        <location filename="../src/contentpane.ui" line="388"/>
        <source>Enroll</source>
        <translation>录入</translation>
    </message>
    <message>
        <location filename="../src/contentpane.ui" line="409"/>
        <location filename="../src/contentpane.cpp" line="329"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../src/contentpane.ui" line="395"/>
        <source>Verify</source>
        <translation>验证</translation>
    </message>
    <message>
        <location filename="../src/contentpane.ui" line="402"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <source>Clear All</source>
        <translation type="vanished">清空所有</translation>
    </message>
    <message>
        <source>Feature name</source>
        <translation type="vanished">特征名称</translation>
    </message>
    <message>
        <source>index</source>
        <translation type="vanished">索引</translation>
    </message>
    <message>
        <source>Device is not connected</source>
        <translation type="vanished">设备未连接</translation>
    </message>
    <message>
        <source>Device is available</source>
        <translation type="vanished">设备可用</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="69"/>
        <source>Opened</source>
        <translation>开</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="72"/>
        <source>Closed</source>
        <translation>关</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="181"/>
        <source>New Feature</source>
        <translation>新的特征</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="182"/>
        <source>Please input a name for the feature:</source>
        <translation>请输入特征名称：</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="184"/>
        <source>Rename Feature</source>
        <translation>特征重命名</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="189"/>
        <source>Duplicate feature name</source>
        <translation>特征名称重复</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="191"/>
        <source>Empty feature name</source>
        <translation>空的特征名称</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="255"/>
        <source>Confirm whether clean all the features?</source>
        <translation>确认是否清除所有特征？</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="256"/>
        <source>Confirm Clean</source>
        <translation>确认清空</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="259"/>
        <source>Confirm whether delete the features selected?</source>
        <translation>确认是否删除选中的特征？</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="260"/>
        <source>Confirm Delete</source>
        <translation>确认删除</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="317"/>
        <source>Delete successfully</source>
        <translation>删除成功</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="330"/>
        <source>The result of delete:</source>
        <translation>删除结果：</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="355"/>
        <source>Clean Failed: </source>
        <translation type="unfinished">清空失败：</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="357"/>
        <source>Clean successfully</source>
        <translation>清空成功</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="460"/>
        <source>DBus calling error</source>
        <translation>DBus调用发生错误</translation>
    </message>
    <message>
        <source>Delete all selected features successfully</source>
        <translation type="vanished">所有选择的特征删除成功</translation>
    </message>
    <message>
        <source>Delete Result</source>
        <translation type="vanished">删除结果</translation>
    </message>
    <message>
        <source>Clean Successfully</source>
        <translation type="vanished">清空成功</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="360"/>
        <source>Clean Result</source>
        <translation>清空结果</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="438"/>
        <source>Rename Successfully</source>
        <translation>重命名成功</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="445"/>
        <source>Rename Result</source>
        <translation>重命名结果</translation>
    </message>
    <message>
        <source>Delete Failed</source>
        <translation type="vanished">删除失败</translation>
    </message>
    <message>
        <source>Clean Failed</source>
        <translation type="vanished">清空失败</translation>
    </message>
    <message>
        <source>Rename Failed</source>
        <translation type="vanished">重命名失败</translation>
    </message>
    <message>
        <source>Fingerprint</source>
        <translation type="vanished">指纹</translation>
    </message>
    <message>
        <source>Fingervein</source>
        <translation type="vanished">指静脉</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="vanished">虹膜</translation>
    </message>
    <message>
        <source>FingerPrint</source>
        <translation type="vanished">指纹</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="vanished">指静脉</translation>
    </message>
    <message>
        <source>Hardware Verification</source>
        <translation type="vanished">硬件验证</translation>
    </message>
    <message>
        <source>Software Verification</source>
        <translation type="vanished">软件验证</translation>
    </message>
    <message>
        <source>Mix Verification</source>
        <translation type="vanished">混合验证</translation>
    </message>
    <message>
        <source>Other Verification</source>
        <translation type="vanished">其他验证</translation>
    </message>
    <message>
        <source>Serial</source>
        <translation type="vanished">串口</translation>
    </message>
    <message>
        <source>USB</source>
        <translation type="vanished">USB</translation>
    </message>
    <message>
        <source>PCIE</source>
        <translation type="vanished">PCIE</translation>
    </message>
    <message>
        <source>Device Storage</source>
        <translation type="vanished">设备存储</translation>
    </message>
    <message>
        <source>OS Storage</source>
        <translation type="vanished">系统存储</translation>
    </message>
    <message>
        <source>Mix Storage</source>
        <translation type="vanished">混合存储</translation>
    </message>
    <message>
        <source>Hardware Identification</source>
        <translation type="vanished">硬件识别</translation>
    </message>
    <message>
        <source>Software Identification</source>
        <translation type="vanished">软件识别</translation>
    </message>
    <message>
        <source>Mix Identification</source>
        <translation type="vanished">混合识别</translation>
    </message>
    <message>
        <source>Other Identification</source>
        <translation type="vanished">其他识别</translation>
    </message>
    <message>
        <source> list</source>
        <translation type="vanished">列表</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="108"/>
        <source>List</source>
        <translation>列表</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Please input a feature name</source>
        <translation type="vanished">请输入特征名称</translation>
    </message>
    <message>
        <source>Feature Rename</source>
        <translation type="vanished">特征重命名</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="185"/>
        <source>Please input a new name for the feature:</source>
        <translation>请输入特征的新名称：</translation>
    </message>
    <message>
        <source>Permission is required. Please authenticate yourself to continue</source>
        <translation type="vanished">需要授权！请先进行认证以继续操作</translation>
    </message>
    <message>
        <source>Enroll successfully</source>
        <translation type="vanished">录入成功</translation>
    </message>
    <message>
        <source>D-Bus calling error</source>
        <translation type="vanished">D-Bus 调用错误</translation>
    </message>
    <message>
        <source>Failed to enroll</source>
        <translation type="vanished">录入失败</translation>
    </message>
    <message>
        <source>Device encounters an error</source>
        <translation type="vanished">设备遇到错误</translation>
    </message>
    <message>
        <source>Operation timeout</source>
        <translation type="vanished">操作超时</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="468"/>
        <source>Device is busy</source>
        <translation>设备忙</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="472"/>
        <source>No such device</source>
        <translation>设备不存在</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="476"/>
        <source>Permission denied</source>
        <translation>没有权限</translation>
    </message>
    <message>
        <source>Search Result</source>
        <translation type="vanished">搜索结果</translation>
    </message>
    <message>
        <source>Failed to get notify message</source>
        <translation type="vanished">读取操作信息失败</translation>
    </message>
    <message>
        <source>In progress, please wait...</source>
        <translation type="vanished">操作中，请稍后...</translation>
    </message>
    <message>
        <source>Match successfully</source>
        <translation type="vanished">匹配成功</translation>
    </message>
    <message>
        <source>Not Match</source>
        <translation type="vanished">不匹配</translation>
    </message>
    <message>
        <source>Failed to match</source>
        <translation type="vanished">匹配失败</translation>
    </message>
    <message>
        <source>Found the matching features:</source>
        <translation type="vanished">搜索到匹配的特征:</translation>
    </message>
    <message>
        <source>Found the matching features:
</source>
        <translation type="obsolete">搜索到匹配的特征:</translation>
    </message>
    <message>
        <source>Found the matching features: 
</source>
        <translation type="vanished">查找到匹配的特征:</translation>
    </message>
    <message>
        <source>Found the matching features name: </source>
        <translation type="vanished">搜索到匹配的特征名称：</translation>
    </message>
    <message>
        <source>No matching features Found</source>
        <translation type="vanished">未搜索到匹配的特征</translation>
    </message>
    <message>
        <source>Found! Username: %1, Feature name: %2</source>
        <translation type="vanished">搜索成功！用户名：%1，特征名称：%2</translation>
    </message>
    <message>
        <source>Not Found</source>
        <translation type="vanished">未搜索到</translation>
    </message>
</context>
<context>
    <name>EnumToString</name>
    <message>
        <location filename="../src/customtype.cpp" line="84"/>
        <location filename="../src/customtype.cpp" line="86"/>
        <source>FingerPrint</source>
        <translation>指纹</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="88"/>
        <source>Iris</source>
        <translation>虹膜</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="90"/>
        <source>Face</source>
        <translation>人脸</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="92"/>
        <source>VoicePrint</source>
        <translation>声纹</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="100"/>
        <source>Hardware Verification</source>
        <translation>硬件验证</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="102"/>
        <source>Software Verification</source>
        <translation>软件验证</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="104"/>
        <source>Mix Verification</source>
        <translation>混合验证</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="106"/>
        <source>Other Verification</source>
        <translation>其他验证</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="114"/>
        <source>Device Storage</source>
        <translation>设备存储</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="116"/>
        <source>OS Storage</source>
        <translation>系统存储</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="118"/>
        <source>Mix Storage</source>
        <translation>混合存储</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="126"/>
        <source>Serial</source>
        <translation>串口</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="128"/>
        <source>USB</source>
        <translation>USB</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="130"/>
        <source>PCIE</source>
        <translation>PCIE</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="132"/>
        <source>Any</source>
        <translation>任意类型</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="134"/>
        <source>Other</source>
        <translation>其他</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="142"/>
        <source>Hardware Identification</source>
        <translation>硬件识别</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="144"/>
        <source>Software Identification</source>
        <translation>软件识别</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="146"/>
        <source>Mix Identification</source>
        <translation>混合识别</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="148"/>
        <source>Other Identification</source>
        <translation>其他识别</translation>
    </message>
</context>
<context>
    <name>InputDialog</name>
    <message>
        <location filename="../src/inputdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/inputdialog.ui" line="65"/>
        <source>InputDialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/inputdialog.ui" line="200"/>
        <source>OK</source>
        <translation>确认</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Biometric Manager</source>
        <translation type="vanished">生物特征管理工具</translation>
    </message>
    <message>
        <source>Icon</source>
        <translation type="vanished">图标</translation>
    </message>
    <message>
        <source>Dashboard</source>
        <translation type="vanished">主面板</translation>
    </message>
    <message>
        <source>Authentication Management</source>
        <translation type="vanished">认证管理</translation>
    </message>
    <message>
        <source>Biometric Authentication Status:</source>
        <translation type="vanished">生物识别认证状态：</translation>
    </message>
    <message>
        <source>Biometric Authentication can take over system authentication processes which include Login, Screensaver, sudo/su and Polkit.</source>
        <translation type="vanished">生物识别可接管系统认证过程，包括登录、锁屏、sudo/su 授权和 Polkit 提权。</translation>
    </message>
    <message>
        <source>Biometric authentication is enabled only when the biometric identification is opened, existing available devices are turned on and the user has enrolled the feature.</source>
        <translation type="vanished">只用当开启了生物识别、存在可用设备而且该用户已经录入了特征才会启用生物识别认证</translation>
    </message>
    <message>
        <source>Device Driver Management</source>
        <translation type="vanished">设备驱动管理</translation>
    </message>
    <message>
        <source>Fingerprint Devices Driver</source>
        <translation type="vanished">指纹设备驱动</translation>
    </message>
    <message>
        <source>Fingervein Devices Driver</source>
        <translation type="vanished">指静脉设备驱动</translation>
    </message>
    <message>
        <source>Iris Devices Driver</source>
        <translation type="vanished">虹膜设备驱动</translation>
    </message>
    <message>
        <source>Device Management</source>
        <translation type="vanished">设备管理</translation>
    </message>
    <message>
        <source>Fingerprint Devices</source>
        <translation type="vanished">指纹设备</translation>
    </message>
    <message>
        <source>Fingervein Devices</source>
        <translation type="vanished">指静脉设备</translation>
    </message>
    <message>
        <source>Iris Devices</source>
        <translation type="vanished">虹膜设备</translation>
    </message>
    <message>
        <source>Fingerprint</source>
        <translation type="vanished">指纹</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="26"/>
        <source>BiometricManager</source>
        <translation>生物特征管理</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="60"/>
        <source>Biometirc Manager</source>
        <translation>生物特征管理</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="87"/>
        <source>UserName</source>
        <translation>用户名</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="203"/>
        <source>DashBoard</source>
        <translation>主界面</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="222"/>
        <location filename="../src/mainwindow.cpp" line="456"/>
        <source>FingerPrint</source>
        <translation>指纹</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="241"/>
        <location filename="../src/mainwindow.cpp" line="456"/>
        <source>FingerVein</source>
        <translation>指静脉</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="279"/>
        <location filename="../src/mainwindow.cpp" line="457"/>
        <source>VoicePrint</source>
        <translation>声纹</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="397"/>
        <source>Biometric Verification Status:</source>
        <translation>生物识别状态：</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="442"/>
        <location filename="../src/mainwindow.cpp" line="478"/>
        <source>Closed</source>
        <translation>关</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="489"/>
        <source>Note</source>
        <translation></translation>
    </message>
    <message>
        <source>     Devices Type</source>
        <translation type="vanished">    设备类型</translation>
    </message>
    <message>
        <source>     All Devices</source>
        <translation type="vanished">所有设备</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="676"/>
        <location filename="../src/mainwindow.ui" line="715"/>
        <location filename="../src/mainwindow.ui" line="766"/>
        <location filename="../src/mainwindow.ui" line="805"/>
        <source>Driver Not Found</source>
        <translation>驱动未找到</translation>
    </message>
    <message>
        <source>Fingervein</source>
        <translation type="vanished">指静脉</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="260"/>
        <location filename="../src/mainwindow.cpp" line="457"/>
        <source>Iris</source>
        <translation>虹膜</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="74"/>
        <location filename="../src/mainwindow.cpp" line="93"/>
        <location filename="../src/mainwindow.cpp" line="614"/>
        <source>Fatal Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="75"/>
        <source>the biometric-authentication service was not started</source>
        <translation>&apos;biometric-authentication&apos;服务没有启动</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="94"/>
        <source>API version is not compatible</source>
        <translation>API 版本不兼容</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="205"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="472"/>
        <source>Opened</source>
        <translation>开</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="473"/>
        <source>Biometric Authentication can take over system authentication processes which include Login, LockScreen, sudo/su and Polkit</source>
        <translation>生物识别可进行系统认证，包括登录、锁屏、sudo/su 授权和 Polkit 提权。</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="479"/>
        <source>There is no any available biometric device or no features enrolled currently.</source>
        <translation>当前没有可用的生物识别设备，或者当前用户没有录入任何特征。</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="522"/>
        <source>Device Name</source>
        <translation>设备名称</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="522"/>
        <source>Status</source>
        <translation>状态</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="615"/>
        <source>Fail to change device status</source>
        <translation>更该设备状态失败</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="198"/>
        <location filename="../src/mainwindow.cpp" line="620"/>
        <source>Restart Service</source>
        <translation>重启服务</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="623"/>
        <source>  Restart immediately  </source>
        <translation>立即重启</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="624"/>
        <source>  Restart later  </source>
        <translation>稍后重启</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="621"/>
        <source>The configuration has been modified. Restart the service immediately to make it effecitve?</source>
        <translation>配置修改成功，是否立即重启服务使其生效？</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="659"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="660"/>
        <source>Device is not connected</source>
        <translation>设备未连接</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="497"/>
        <source>Warnning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="498"/>
        <source>There is no available device or no features enrolled</source>
        <translation>没有可用设备或者没有录入特征</translation>
    </message>
</context>
<context>
    <name>MessageDialog</name>
    <message>
        <location filename="../src/messagedialog.ui" line="26"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/messagedialog.ui" line="74"/>
        <source>MessageDialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/messagedialog.ui" line="145"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/messagedialog.ui" line="217"/>
        <source>OK</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../src/messagedialog.ui" line="233"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>PromptDialog</name>
    <message>
        <location filename="../src/promptdialog.ui" line="26"/>
        <source>Current Progress</source>
        <translation>当前进度</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.ui" line="77"/>
        <source>PromptDialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/promptdialog.ui" line="138"/>
        <location filename="../src/promptdialog.ui" line="199"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Operations are in progress. Please wait...</source>
        <translation type="vanished">操作中，请稍后...</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="52"/>
        <source>Enroll</source>
        <translation>录入</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="55"/>
        <source>Verify</source>
        <translation>验证</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="58"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="92"/>
        <source>In progress, please wait...</source>
        <translation>操作中，请稍后...</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="104"/>
        <location filename="../src/promptdialog.cpp" line="106"/>
        <source>Index</source>
        <translation>序列号</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="104"/>
        <source>UserName</source>
        <translation>用户名</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="104"/>
        <location filename="../src/promptdialog.cpp" line="106"/>
        <source>FeatureName</source>
        <translation>特征名称</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="162"/>
        <source>Permission is required.
Please authenticate yourself to continue</source>
        <translation>需要授权！请先进行认证以继续操作</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="190"/>
        <source>Enroll successfully</source>
        <translation>录入成功</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="226"/>
        <source>Verify successfully</source>
        <translation>验证成功</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="228"/>
        <source>Not Match</source>
        <translation>不匹配</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="261"/>
        <source>Search Result</source>
        <translation>搜索结果</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="277"/>
        <source>No matching features Found</source>
        <translation>未搜索到匹配的特征</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="341"/>
        <source>D-Bus calling error</source>
        <translation>D-Bus 调用错误</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="353"/>
        <source>Device encounters an error</source>
        <translation>设备遇到错误</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="360"/>
        <source>Operation timeout</source>
        <translation>操作超时</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="367"/>
        <source>Device is busy</source>
        <translation>设备忙</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="371"/>
        <source>No such device</source>
        <translation>设备不存在</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="375"/>
        <source>Permission denied</source>
        <translation>没有权限</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="384"/>
        <source>Failed to enroll</source>
        <translation>录入失败</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="387"/>
        <source>Failed to match</source>
        <translation>匹配失败</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="390"/>
        <source>Not Found</source>
        <translation>未搜索到</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="403"/>
        <source>&lt;font size=&apos;2&apos;&gt;the window will be closed after two second&lt;/font&gt;</source>
        <translation>&lt;font size=&apos;2&apos;&gt;窗口将在两秒后关闭&lt;/font&gt;</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/main.cpp" line="15"/>
        <source>Username</source>
        <translation>用户名</translation>
    </message>
    <message>
        <source>FingerPrint</source>
        <translation type="vanished">指纹</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="vanished">指静脉</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="vanished">虹膜</translation>
    </message>
</context>
<context>
    <name>TreeModel</name>
    <message>
        <source>FingerPrint</source>
        <translation type="vanished">指纹</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="vanished">指静脉</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="vanished">虹膜</translation>
    </message>
    <message>
        <source>Face</source>
        <translation type="vanished">人脸特征</translation>
    </message>
    <message>
        <source>VoicePrint</source>
        <translation type="vanished">声纹</translation>
    </message>
    <message>
        <location filename="../src/treemodel.cpp" line="11"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../src/treemodel.cpp" line="13"/>
        <location filename="../src/treemodel.cpp" line="15"/>
        <source>index</source>
        <translation>序列号</translation>
    </message>
    <message>
        <location filename="../src/treemodel.cpp" line="13"/>
        <source>username</source>
        <translation>用户名</translation>
    </message>
</context>
</TS>
